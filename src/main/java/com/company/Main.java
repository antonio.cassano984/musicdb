package com.company;

import java.net.URL;
import java.sql.*;

public class Main {

    private static final URL resource = Main.class.getClassLoader().getResource("music.db");

    public static void main(String[] args) {
        try(Connection connection = DriverManager.getConnection("jdbc:sqlite:"+resource);
            Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery("select * from songs");

            while(resultSet.next()) {
                System.out.println(resultSet.getInt("_id") + "|" +
                        resultSet.getInt("track") + "|" +
                        resultSet.getString("title") + "|" +
                        resultSet.getInt("album"));
            }

        } catch(SQLException e) {
            System.out.println("Something went wrong" + e.getMessage());
            e.printStackTrace();
        }

    }
}
